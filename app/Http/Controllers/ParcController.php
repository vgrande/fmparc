<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; // importation de la classe Request
use App\Parc; // importation du modèle Parc pour les requête en BDD
use App\Modele; // importation du modèle Modele pour les requête en BDD
use App\Site; // importation du modèle Site pour les requête en BDD
use App\Utilisateur; // importation du modèle Utilisateur pour les requête en BDD
use Codedge\Fpdf\Fpdf\Fpdf; // importation de la classe Fpdf pour la génération de fiches au format PDF

class ParcController extends Controller
{
    public function index($id = 0){
        /* Si aucun id n'est passé en paramêtre via l'URL on retourne tous les enregistrements du parc,
        les modèles de PC actifs, les modèles inactifs ainsi que tous les sites.
        Ces variables envoyées au template permettrons d'afficher les infos sur la page web.*/ 
        if($id == 0){
            return view('index', [
                'parc' => Parc::all(),
                'modeles' => Modele::select()->where('actif',1)->get(),
                'modelesinactifs' => Modele::select()->where('actif',0)->get(),
                'sites' => Site::all(),
            ]);
        }
        /* Si un id est passé en paramêtre via l'URL on retourne tous les enregistrements du parc,
        les modèles de PC actifs, les modèles inactifs ainsi que tous les sites.
        On envoie également une variable qui contient toutes les infos dont l'identifiant est passé en paramêtres, pour préremplir le formulaire.
        Ces variables envoyées au template permettrons d'afficher les infos sur la page web.*/ 
        else{
            return view('index', [
                'parc' => Parc::all(),
                'modeles' => Modele::select()->where('actif',1)->get(),
                'modelesinactifs' => Modele::select()->where('actif',0)->get(),
                'sites' => Site::all(),
                'modif' => Parc::select()->where('id',$id)->get(),
            ]);
        }
    }
   /* La fonction ci-dessous permet d'enregistrer un PC dans le parc, ainsi qu'enregistrer l'utilisateur si il n'existe pas encore dans l'outil , elle correspond au traitement du formulaire "Enregistrer un PC"*/
    public function enregistrer(Request $request){ // Prend en paramêtres un objet de type request, qui correspond a un objet contenant tous les champs envoyés lors de la soumission du formulaire.
        if(!empty($request->nom) && !empty($request->prenom) && !empty($request->login) && !empty($request->site) && !empty($request->modele) && !empty($request->numSerie)){ // on verifie que les différents attributs ne sont pas vides pour commencer le traitement
            $utilisateur = Utilisateur::select()->where('login', strtolower($request->login))->get(); // On cherche l'utilisateur dans la BDD
            if(!isset($utilisateur[0]['id'])){ //Si l'utilisateur n'existe pas on le créé
                $utilisateur = new Utilisateur; //On créé donc un objet de type Utilisateur puis on lui défini des attributs nom, prenom, login
                $utilisateur->nom = strtolower($request->nom);
                $utilisateur->prenom = strtolower($request->prenom);
                $utilisateur->login = strtolower($request->login);
                $utilisateur->save(); // Enfin on enregistre l'utilisateur en BDD
                $utilisateur = Utilisateur::select()->where('login', strtolower($request->login))->get(); // On séléctionne à nouveau l'utilisateur dans la BDD pour avoir ses informations qui nous servirons par la suite.
            }
            $parc = Parc::select()->where('numSerie', $request->numSerie)->get(); // On cherche dans le parc si le PC existe déjà
            if(isset($parc[0]['numSerie']) && $parc[0]['numSerie'] ==$request->numSerie){ // Dans ce premier cas le PC existe déjà dans le parc donc nous allons modifier seulement certains attributs
                Parc::where('numSerie', $request->numSerie)->update(['dateAttrib'=>date("Y-m-d"),'idSite'=>$request->site,'idUtilisateur'=>$utilisateur[0]['id']]); // On met à jour les infos du PC dans le parc : nouvelle date d'attribution, nouvel identifiant de site ainsi que l'id du nouvel utilisateur
            }else{ // Dans ce deuxième cas, le PC n'existe pas encore dans le parc
                $enregistrement = new Parc; // On créé donc un objet de type Parc puis on lui défini des attributs dateAttrib, dateFinGarantie, idSite, idModele, idUtilisateur et numSerie
                $enregistrement->dateAttrib = date("Y-m-d");
                $enregistrement->dateFinGarantie = date("Y-m-d", strtotime('+3 year'));
                $enregistrement->idSite = $request->site;
                $enregistrement->idModele = $request->modele;
                $enregistrement->idUtilisateur = $utilisateur[0]['id'];
                $enregistrement->numSerie = $request->numSerie;
                $enregistrement->save(); // Enfin on enregistre le PC dans le parc
            
            }
            if($request->fiche == True){ // Vérifie si il faut également générer une fiche nouvel utilisateur, si c'est le cas on appelle la fonction makePDF en lui passant les informations nécessaires en paramètres.
                $this->makePDF($request->nom, $request->prenom,$request->login,$request->mail,Modele::select()->where('id',$request->modele)->get());
            }else{
                return redirect()->route("parc"); // Sinon on rafraichi la page
            }
        }
        return redirect()->route("parc"); // Si l'utilisateur soumet un formulaire vide ou avec des données manquantes, on ne fait pas le traitement et on rafraichi directement la page.
    }
    /*La fonction ci-dessous permet de générer une fiche nouvel utilisateur, elle correspond au traitement du formulaire "Générer une fiche" */
    public function fiche(Request $request){ 
        if(!empty($request->nom) && !empty($request->prenom) && !empty($request->login) && !empty($request->mail) && !empty($request->modele)){ // vérifie que les champs du formulaire soient tous remplis
            $this->makePDF($request->nom, $request->prenom, $request->login, $request->mail,Modele::select()->where('id',$request->modele)->get()); // appel de la fonction makePDF pour générer le PDF en lui passant les différentes infos en paramètres
        }
    }
    
    public function nouveaumodele(Request $request){
        $modele = Modele::select()->where('libModele',$request->nouveaumodele)->get();
        if(!isset($modele[0]['libModele']) && !empty($request->nouveaumodele)){
            $modele = new Modele;
            $modele->libModele = $request->nouveaumodele;
            $modele->actif = 1;
            $modele->save();
        }
        return redirect()->route('parc');
    }

    public function desactivermodele(Request $request){
        Modele::select()->where('id',$request->modele)->update(['actif'=>0]);
        return redirect()->route('parc');
    }

    public function activermodele(Request $request){
        Modele::select()->where('id',$request->modele)->update(['actif'=>1]);
        return redirect()->route('parc');
    }

    public function supprimer(Request $request){
        $pc = Parc::select()->where('numSerie', $request->numSerie)->get();
        if(!empty($pc[0]['numSerie']) && $pc[0]['numSerie'] == $request->numSerie){
            Parc::where('numSerie', $request->numSerie)->delete();
        }
        return redirect()->route('parc');
    }

    private $fpdf;
    public function makePDF($nom, $prenom, $nomutil,$mail,$modelepc){
        for($i = 0; $i<strlen($nom);$i++){
            $nom=str_replace(" ","",$nom);
        }
        
        for($i = 0; $i<strlen($nomutil)+1;$i++){
            $nomutil=str_replace("é","e",$nomutil);
            $nomutil=str_replace("à","a",$nomutil);
            $nomutil=str_replace("ë","e",$nomutil);
            $nomutil=str_replace("É","e",$nomutil);
        }
        $nomutil=strtolower($nomutil);
        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage();
        $this->fpdf->Image('./img/banner.PNG', -1,-1);
        $this->fpdf->SetFont('Arial', '', 17);
        $this->fpdf->Text(10, 70, utf8_decode("Bonjour $prenom $nom,"));
        $this->fpdf->SetFont('Arial', 'B', 16);
        $this->fpdf->Image('./img/deuxpostes.PNG', 185,60);
        $this->fpdf->SetFont('Arial', 'B', 16);
        $this->fpdf->SetTextColor(0,66,255);
        $this->fpdf->Text(10, 85, utf8_decode("Bienvenue sur le réseau d'entreprise FM Logistic."));
        $this->fpdf->SetTextColor(0,0,0);
        $this->fpdf->SetFont('Arial', '', 15);
        $this->fpdf->Text(10, 95, utf8_decode("Voici la liste des équipements informatiques mis à votre disposition :"));
        $this->fpdf->SetFont('Arial', '', 14);
        $this->fpdf->Text(10, 105, utf8_decode("- Un PC portable ".$modelepc[0]['libModele']));
        $this->fpdf->Text(10, 110, utf8_decode("- Une sacoche de transport ou sac à dos"));
        $this->fpdf->Text(10, 115, utf8_decode("- Une souris"));
        $this->fpdf->Text(10, 120, utf8_decode("- En option : clavier / écran(s) / divers"));
        $this->fpdf->SetFont('Arial', '', 15);
        $this->fpdf->Text(10, 130, utf8_decode("Votre ordinateur est d'ores et déjà configuré et prêt à l'emploi."));
        $this->fpdf->Text(10, 140, utf8_decode("Veuillez trouver ci-dessous les informations de connexion propre à votre compte :"));
        $this->fpdf->SetTextColor(0,66,255);
        $this->fpdf->SetFont('Arial', 'B', 15);
        $this->fpdf->Text(10, 150, utf8_decode("Session Windows"));
        $this->fpdf->SetTextColor(0,0,0);
        $this->fpdf->SetFont('Arial', '', 14);
        $this->fpdf->Text(10, 160, utf8_decode("nom d'utilisateur: $nomutil"));
        $this->fpdf->Text(10, 165, utf8_decode("Mot de passe : FMLogistic1$ (à modifier à votre première connexion*)"));
        $this->fpdf->SetFont('Arial', '', 15);
        $this->fpdf->SetTextColor(0,66,255);
        $this->fpdf->SetFont('Arial', 'B', 15);
        $this->fpdf->Text(10, 175, utf8_decode("Adresse de messagerie Google"));
        $this->fpdf->SetTextColor(0,0,0);
        $this->fpdf->SetFont('Arial', '', 14);
        $this->fpdf->Text(10, 185, utf8_decode("Nom d'utilisateur : $mail"));
        $this->fpdf->Text(10, 190, utf8_decode("Mot de passe : Fourni par le service RH."));
        $this->fpdf->SetFont('Arial', '', 15);
        $this->fpdf->SetTextColor(0,66,255);
        $this->fpdf->SetFont('Arial', 'B', 15);
        $this->fpdf->Text(10, 200, utf8_decode("Compte VPN ( Accès au réseau interne à distance en option )"));
        $this->fpdf->SetTextColor(0,0,0);
        $this->fpdf->SetFont('Arial', '', 14);
        $this->fpdf->Text(10, 210, utf8_decode("Nom d'utilisateur : $nomutil"));
        $this->fpdf->Text(10, 215, utf8_decode("Mot de passe : Le mot de passe Windows."));
        $this->fpdf->SetFont('Arial', '', 15);
        $this->fpdf->SetTextColor(0,66,255);
        $this->fpdf->SetFont('Arial', 'B', 15);
        $this->fpdf->Text(10, 225, utf8_decode("Compte Intranet"));
        $this->fpdf->SetTextColor(0,0,0);
        $this->fpdf->SetFont('Arial', '', 14);
        for($i = 0; $i<strlen($prenom);$i++){
            $prenom=str_replace("é","e",strtolower($prenom));
            $prenom=str_replace("à","a",strtolower($prenom));
            $prenom=str_replace("ë","e",$prenom);
            $prenom=str_replace("-"," ",$prenom);
        }
        for($i = 0; $i<strlen($nom);$i++){
            $nom=str_replace("é","e",strtolower($nom));
            $nom=str_replace("à","a",strtolower($nom));
            $nom=str_replace("ë","e",$nom);
        }
        $this->fpdf->Text(10, 235, utf8_decode("Nom d'utilisateur : $prenom $nom"));
        $this->fpdf->Text(10, 245, utf8_decode("Mot de passe :  FMLogistic1$ (à modifier à votre première connexion*)"));
        $this->fpdf->SetFont('Arial', '', 12);
        $this->fpdf->Text(10, 265, utf8_decode(" Pour toutes demandes de support informatiques, nous sommes à votre disposition au :	"));
        $this->fpdf->Text(10, 270, utf8_decode(" 03 87 23 12 12, le 1199 en interne ou via Request Tracker."));
        $this->fpdf->Image('./img/footer.PNG', -1,280);
        $this->fpdf->AddPage();
        $this->fpdf->Image('./img/zscaler.png', -10, 1, 230, 300);
        $this->fpdf->AddPage();
        $this->fpdf->Image('./img/apputiles.png', -10, 1, 230, 300);
        $this->fpdf->AddPage();
        $this->fpdf->Image('./img/meet.png', -20, 1, 250, 300);
        $this->fpdf->AddPage();
        $this->fpdf->Image('./img/FP.png', -10, 1, 240, 300);
        return $this->fpdf->Output();
    }
}

